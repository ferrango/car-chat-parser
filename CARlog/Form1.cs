﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace CARlog
{
	public partial class Form1 : Form
	{
		private string filePath;
		Dictionary<string, Dictionary<string, int>> wordsPerUser;
		List<string> uselessLines;
		string[] dInfo = null;

		public Form1()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{

			progressBar1.Value = 0;
			if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
			{
				filePath = folderBrowserDialog1.SelectedPath;
				toolStripStatusLabel1.Text = "Ready.";
			}
			else
			{
				filePath = null;

				toolStripStatusLabel1.Text = "Awaiting directory input.";
			}
		}

		private void button2_Click(object sender, EventArgs e)
		{
			if (!String.IsNullOrWhiteSpace(filePath))
			{
				if (Directory.GetFiles(filePath, "*.txt").Any())
				{
					backgroundWorker1.RunWorkerAsync();
					progressBar1.Style = ProgressBarStyle.Marquee;
				}
				else MessageBox.Show("No log files detected in destination directory. Make sure that chat log files end with a text extension (.txt)");

			}
			else MessageBox.Show("No source directory specified!");
		}

		private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
		{
			string[] dInfo = Directory.GetFiles(filePath, "*.txt");
			wordsPerUser = new Dictionary<string, Dictionary<string, int>>();
			uselessLines = new List<string>();

			for (int i = 0; i < dInfo.Length; i++)
			{
				(sender as BackgroundWorker).ReportProgress(1, String.Format("Processing file {0} of {1}", i + 1, dInfo.Length));
				ProcessFile(dInfo[i]);
			}

		}

		private void ProcessFile(string fileName)
		{
			using (StreamReader sr = new StreamReader(fileName))
			{
				while (!sr.EndOfStream)
				{
					string chatLine = sr.ReadLine();
					string chatLineOrigin = chatLine;
					if (chatLine.StartsWith("["))
					{
						bool isAction = false;

						// string up to the date
						string date = chatLine.Substring(0, chatLine.IndexOf("]") + 1);

						chatLine = chatLine.Substring(date.Length).Trim();
						string nickname = "";

						if (chatLine.StartsWith("<"))
						{
							nickname = chatLine.Substring(1, chatLine.IndexOf(">") - 1);
							chatLine = chatLine.Substring(nickname.Length + 3);
						}
						else if (chatLine.StartsWith("*"))
						{
							chatLine = chatLine.Substring(2);
							nickname = chatLine.Substring(0, chatLine.IndexOf(" "));
							chatLine = chatLine.Substring(nickname.Length + 1);
							isAction = false;
						}

						if (!String.IsNullOrWhiteSpace(nickname))
						{
							string pattern = @"^(\...|\s+|\d+|\w+|[^\d\s\w])+$";


							List<string> words = new List<string>();

							Regex regex = new Regex(pattern);

							if (regex.IsMatch(chatLine))
							{
								Match match = regex.Match(chatLine);

								foreach (Capture capture in match.Groups[1].Captures)
								{
									string word = capture.Value.ToLower();
									if (!wordsPerUser.ContainsKey(word))
										wordsPerUser[word] = new Dictionary<string, int>();

									if (!wordsPerUser[word].ContainsKey(nickname))
										wordsPerUser[word][nickname] = 1;
									else wordsPerUser[word][nickname]++;
								}
							}
						}
						else uselessLines.Add(chatLineOrigin);
					}
				}
			}
		}

		private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			progressBar1.Style = ProgressBarStyle.Continuous;
			progressBar1.Value = 100;
			toolStripStatusLabel1.Text = "Log processing completed.";
		}

		private void button3_Click(object sender, EventArgs e)
		{
			toolStripStatusLabel1.Text = "Making file.";
			List<string> allNicks = wordsPerUser.SelectMany(x => x.Value.Keys.ToList()).Distinct().OrderBy(x => x).ToList();
			List<string> allWords = wordsPerUser.Keys.OrderBy(x=>x).ToList();

			allWords.Remove(",");

			using (StreamWriter sw = new StreamWriter(new FileStream("output.csv", FileMode.Create) , Encoding.Default))
			{

				sw.WriteLine(String.Format("USERNAME,{0}", String.Join(",", allWords)));

				foreach (var name in allNicks)
				{
					string toWriteNext = String.Format("{0},", name);
					foreach (var word in allWords)
					{
						toWriteNext += wordsPerUser[word].ContainsKey(name) ?
							 wordsPerUser[word][name].ToString() + "," : "0,";
					}

					toWriteNext.Remove(toWriteNext.Length - 1);
					sw.WriteLine(toWriteNext);
				}

				sw.Flush();
			}

			File.WriteAllLines("errors.txt", uselessLines);


				toolStripStatusLabel1.Text = "CSV file created.";
		}

		private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			toolStripStatusLabel1.Text = e.UserState as string;
		}


	}
}
